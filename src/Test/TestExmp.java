package Test;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import Main.Book;
import Main.BookServicesArrayImpl;
import Main.Type;

class TestExmp {
	BookServicesArrayImpl bookServiceArrayImpl=new BookServicesArrayImpl(3);

	@Test
	void test() {
		bookServiceArrayImpl.add(new Book("action",900,600,Type.valueOf("HORROR"),LocalDate.parse("2012-12-11")));
		bookServiceArrayImpl.add(new Book("hunger",1200,400,Type.valueOf("HORROR"),LocalDate.parse("2012-11-11")));
		assertEquals(1,bookServiceArrayImpl.getClass());
	
	}
	
}
