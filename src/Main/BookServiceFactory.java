package Main;

import java.util.Properties;

public abstract class BookServiceFactory {
	public static BookServices getBookServices(String type, Properties properties) {
		BookServices service = null;
		switch (type) {
		case "Array":
			service = new BookServicesArrayImpl();
			break;
		case "ArrayList":
			service = new BookSerivesArrayListImpl();
			break;
		case "Database":
			String driver = properties.getProperty("driver");

			String url = properties.getProperty("url");

			String user = properties.getProperty("user");

			String password = properties.getProperty("password");
			service = new BookServiceDatabaseImpl(driver, url, user, password);

		}
		return service;

	}
}
