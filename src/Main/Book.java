package Main;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Scanner;

public class Book implements Comparable<Book> {
	private String name;
	private int pages;
	private int price;
	private Type type;
	LocalDate publishdate;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public LocalDate getPublishdate() {
		return publishdate;
	}

	public void setPublishdate(LocalDate publishdate) {
		this.publishdate = publishdate;
	}

	public Book(String name, int pages, int price, Type type, LocalDate pubdate) {
		this.name = name;
		this.pages = pages;
		this.price = price;
		this.publishdate = pubdate;
		this.type = type;
	}

	public Book() {
	}

	public String toString() {

		{
			return new StringBuffer().append("NAME:").append(name).append("\n").append("PAGES").append(pages)
					.append("\n").append("PRICE:").append(price).append("\n").append("TYPE").append(type)
					.append(",").append("published date:").append(publishdate).append("\n").toString();

		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		return Objects.equals(name, other.name) && pages == other.pages && price == other.price
				&& Objects.equals(publishdate, other.publishdate) && type == other.type;
	}

	/*
	 * public void update() { Scanner scan = new Scanner(System.in);
	 * System.out.println("enter the name"); name = scan.next();
	 * System.out.println("enter the pages"); pages = scan.nextInt();
	 * System.out.println("enter the price"); price = scan.nextInt();
	 * System.out.println("enter the type"); String type = scan.next();
	 * System.out.println("enter the published date"); String date = scan.next();
	 * publishdate = LocalDate.parse(date); }
	 */

	/*
	 * public void display() { System.out.println("enter the name  :" + name);
	 * System.out.println("enter the pages :" + pages);
	 * System.out.println("enter the price :" + price);
	 * System.out.println("enter the type  :" + type);
	 * System.out.println("enter the published date:" + publishdate);
	 * 
	 * }
	 */

	@Override
	public int compareTo(Book book) {
		// TODO Auto-generated method stub
		return (int) (price - book.price);

	}

}
