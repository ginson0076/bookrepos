package Main;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.StringJoiner;

/**
 * A interface specifies the operations on Student object
 * 
 * @author Ginson
 *
 *
 */
public class BookServicesArrayImpl implements BookServices {
	private Book[] bookArray;
	private int count = 0;

	public BookServicesArrayImpl() {
		bookArray = new Book[100];
	}

	public BookServicesArrayImpl(int size) {
		bookArray = new Book[size];
	}

	/**
	 * The method will add a book
	 * 
	 * @param book book to be added
	 *
	 */

	public void add(Book book) {
		boolean flag = true;
		if (count >= bookArray.length)
			System.out.println("Data Limit exceeded");
		else if (flag == true) {
			for (int i = 0; i < count; i++) {
				if (bookArray[i].equals(book)) {
					System.out.println("Duplicate Data");
					flag = false;
				}
			}
		}
		if (flag == true) {
			bookArray[count] = book;
			count++;
		}
	}

	/**
	 * The method should display all the books
	 */
	public void displayAll() {

		if (count == 0) {
			System.out.println("no data");
		} else
			for (int i = 0; i < count; i++) {
				System.out.println(bookArray[i]);
			}
	}

	/**
	 * The method will search books and display the book with given name
	 * 
	 * @param name name of the book to be searched
	 *
	 */
	public void search(String name) {
		boolean flag;
		flag = false;
		for (int i = 0; i < count; i++) {
			if (bookArray[i].getName().equalsIgnoreCase(name)) {
				System.out.println(bookArray[i]);
				flag = true;
			}

		}
		if (flag = false)
			System.out.println("no data");
	}

	/**
	 * The method will search books and display the book with publish date
	 * 
	 * @param publish date
	 *
	 */
	public List<Book> search(LocalDate date) {
		List<Book> bookList = new ArrayList<Book>();
		for (int i = 0; i < count; i++) {
			if (bookArray[i].getPublishdate().isAfter(date)) {
				bookList.add(bookArray[i]);

			}
		}

		return bookList;
	}

	/**
	 * The method sorts the book based on price
	 */

	public void sort() {
		// TODO Auto-generated method stub
		Arrays.sort(bookArray, 0, count);

		displayAll();
	}

	/* This method download the book details as CSV file */

	public void downloadCSVfile() {
		try {
			FileWriter file = new FileWriter("BookDetails.csv");
			for (int i = 0; i < count; i++) {
				String result = getCSVString(bookArray[i]);
				file.write(result);
			}
			file.close();
		} catch (Exception exception) {
			System.out.println("Exception");
			exception.printStackTrace();
		}
	}

	/* This method download the book details a Json file */
	public void downloadJson() throws IOException {
		String data;

		StringJoiner join = new StringJoiner(",", "[", "]");
		FileWriter file = new FileWriter("download.json");
		for (int i = 0; i < count; i++) {
			data = getJson(bookArray[i]);
			join.add(data);
		}
		file.write(join.toString());
		file.close();
	}

	/* This method will delete the book details */

	@Override
	public void delete(String name) {
		// TODO Auto-generated method stub
		int k = 0;
		for (int i = 0; i < count; i++) {
			if (bookArray[i].getName().equals(name)) {
				bookArray[k] = bookArray[i];
				k++;
				System.out.println("deleted\n");
			}
		}
		count = k;
	}
}
