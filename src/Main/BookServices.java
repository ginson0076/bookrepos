package Main;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public interface BookServices {

	void add(Book book);

	void displayAll() throws Exception;

	void search(String name) throws Exception;

	void delete(String name);

	List<Book> search(LocalDate pubdate) throws Exception;

	void sort() throws Exception;

	default String getCSVString(Book book) {

		return new StringBuffer(book.getName()).append(",").append(book.getPages()).append(",").append(book.getPrice())
				.append(",").append(book.getPublishdate()).append(",").append(book.getType()).toString();
	}

	void downloadCSVfile() throws Exception;

	default String getJson(Book book) {
		return new StringBuffer().append("{").append("\"Name\":").append("\"").append(book.getName()).append("\"")
				.append(",").append("\"Pages\":").append(book.getPages()).append(" ,").append("\"Price\":")
				.append(book.getPrice()).append("\"Publishdate\":").append(book.getPublishdate()).append(",")
				.append("\"Type\":").append(book.getType()).append("}").toString();
	}

	void downloadJson() throws Exception;
}
