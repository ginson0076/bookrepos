package Main;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class BookServiceDatabaseImpl implements BookServices {

	String user, password, driver, url;

	public BookServiceDatabaseImpl(String driver, String url, String user, String password) {
		this.user = user;
		this.password = password;
		this.driver = driver;
		this.url = url;
	}

	Connection getConnection() throws SQLException, ClassNotFoundException {
		Class.forName(driver);
		return DriverManager.getConnection(url, user, password);

	}

	private Book getbook(ResultSet result) throws Exception { 
		Book book = new Book();
		book.setName(result.getString("name"));
		book.setPages(result.getInt("pages"));
		book.setPrice(result.getInt("price"));
		book.setType(Type.valueOf(result.getString("type")));
		book.setPublishdate(result.getDate("published_date").toLocalDate());

		return book;
	}

	@Override
	public void add(Book book) {
		try {
			Connection connection;
			connection = getConnection();
			String query = "Insert into book(name,pages,price,type,published_date)Values(?,?,?,?,?)";
			PreparedStatement preparedStatement = connection.prepareStatement(query);

			preparedStatement.setString(1, book.getName());
			preparedStatement.setInt(2, book.getPages());
			preparedStatement.setInt(3, book.getPrice());
			preparedStatement.setString(4, book.getType().toString());
			preparedStatement.setDate(5, Date.valueOf(book.getPublishdate()));

			preparedStatement.executeUpdate();
			connection.close();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	@Override
	public void displayAll() throws Exception {
		Connection connection;
		try {
			connection = getConnection();

			PreparedStatement preparedStatement = connection.prepareStatement("Select * from book ");
			ResultSet result = preparedStatement.executeQuery();
			while (result.next()) {
				Book book = getbook(result);
				System.out.println(book);
			}
			connection.close();

		} catch (ClassNotFoundException exception) {
			// TODO Auto-generated method stub
			exception.printStackTrace();
		} catch (SQLException exception) {// TODO Auto-generated method stub
			exception.printStackTrace();
		}

	}

	@Override
	public void search(String name) throws Exception {
		// TODO Auto-generated method stub
		try {
			Connection connection;
			connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM book where name=?");
			preparedStatement.setString(1, name);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next())

			{
				{
					Book book = getbook(resultSet);
					System.out.println(book);
				}
			}
			connection.close();

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void delete(String name) {
		// TODO Auto-generated method stub

		try {
			Connection connection = getConnection();
			connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM book where name=?");
			preparedStatement.setString(1, name);
			preparedStatement.executeUpdate();
			System.out.println("...Data deleted from database successfully...");
			connection.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Book> search(LocalDate pubdate) throws Exception {
		List<Book> bookList = new ArrayList<>();
		{
			// TODO Auto-generated method stub
			try {
				Connection connection = getConnection();
				connection = getConnection();
				// String query = "SELECT * FROM player;";
				PreparedStatement preparedStatement = connection
						.prepareStatement("SELECT * FROM book where published_date=?;");
				preparedStatement.setDate(1, Date.valueOf(pubdate));
				ResultSet resultSet = preparedStatement.executeQuery();
				while (resultSet.next()) {
					Book book = getbook(resultSet);
					System.out.println(book);
				}
				connection.close();
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
		}
		return bookList;

	}

	@Override
	public void sort() throws Exception {
		// TODO Auto-generated method stub
		Connection connection;
		try {
			connection = getConnection();
			PreparedStatement prestatement = connection.prepareStatement("select * from book order by price asc");
			ResultSet resultSet = prestatement.executeQuery();
			while (resultSet.next()) {
				Book book = getbook(resultSet);
				System.out.println(book);
			}
			resultSet.close();
			connection.close();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void downloadCSVfile() throws Exception {
		// TODO Auto-generated method stub
		Connection connection;
		try {
			connection = getConnection();
			PreparedStatement prestatement = connection.prepareStatement("select * from book");
			ResultSet resultSet = prestatement.executeQuery();
			FileWriter file = new FileWriter("Book database details.csv");
			while (resultSet.next()) {

				String data = getCSVString(getbook(resultSet));
				file.write(data);
			}
			file.close();
			connection.close();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void downloadJson() throws Exception {
		// TODO Auto-generated method stub

		Connection connection;
		try {
			connection = getConnection();
			PreparedStatement prestatement = connection.prepareStatement("select * from book");
			ResultSet resultSet = prestatement.executeQuery();
			FileWriter file = new FileWriter("BookDetails.json");
			StringJoiner join = new StringJoiner(",", "[", "]");
			while (resultSet.next()) {
				String data = join.add(getJson(getbook(resultSet))).toString();
				file.write(data);
			}
			file.close();
			connection.close();
		} catch (ClassNotFoundException | SQLException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
