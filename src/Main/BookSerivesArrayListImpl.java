package Main;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class BookSerivesArrayListImpl implements BookServices {
	List<Book> bookList = new ArrayList<>();
	int count = 0;

	@Override
	public void add(Book book) {
		if (bookList.size() > 0 && bookList.contains(book)) {
			System.out.println("Already exist");
		} else {

			bookList.add(book);
		}
		count++;
	}

	@Override
	public void displayAll() {
		bookList.forEach(System.out::println);

	}

	@Override
	public void search(String name) {
		System.out.println(
				bookList.stream().filter(book -> book.getName().equalsIgnoreCase(name)).collect(Collectors.toList()));
	}

	// TODO Auto-generated method stub

	@Override
	public List<Book> search(LocalDate pubdate) {
		// TODO Auto-generated method stub

		return bookList.stream().filter(book -> book.getPublishdate().isAfter(pubdate)).collect(Collectors.toList());

	}

	@Override
	public void sort() {
		// TODO Auto-generated method stub
		Collections.sort(bookList);
		displayAll();
	}

	@Override
	public void downloadJson() throws IOException {
		// TODO Auto-generated method stub

		String str;
		StringJoiner join = new StringJoiner(",", "[", "]");
		FileWriter file = new FileWriter("BookServiceImpl.Json");
		for (Book book : bookList) {
			str = getJson(book);
			join.add(str);
		}
		file.write(join.toString());
		file.close();

	}

	@Override
	public void delete(String name1) {
		// TODO Auto-generated method stub
		Iterator<Book> iterator = bookList.iterator();
		Book book=iterator.next();
				if (book.getName().equals(name1)) {
					iterator.remove();
				}

			}
		
	

	@Override
	public void downloadCSVfile() throws IOException {
		// TODO Auto-generated method stub

		FileWriter filewriter = new FileWriter("BookServicecImpl.csv");

		for (int i = 0; i < bookList.size(); i++) {
			String res = getCSVString(bookList.get(i));
			System.out.println(res);
			filewriter.write(res);

		}
		filewriter.close();
	}

}
