package Main;

import java.io.FileNotFoundException;

import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

public class Menu {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);

		FileReader reader = new FileReader("src/config.properties");

		Properties properties = new Properties();
		properties.load(reader);
		// BookServices book1=Book BookServicesArrayImpl();
		// BookServices book1=BookServiceFactory.getBookServices("Array");
		BookServices bookService = BookServiceFactory.getBookServices("Database", properties);
		int d;
		while (true) {
			System.out.println(
					"1.Add Book\n2.display the book details\n3.Search\n4.sort\n5.search by publishdate\n6.download the book details\n7.download the bookdetails as Json\n8.delete\n9.exit");
			System.out.println();
			d = scan.nextInt();

			switch (d) {
			case 1:
				try {
					System.out.println("enter the name:");
					String name = scan.next();
					System.out.println("enter the pages");
					int pages = scan.nextInt();
					System.out.println("enter the price");
					int price = scan.nextInt();
					System.out.println("enter the type");
					String type = scan.next();
					Type type1 = Type.valueOf(type.toUpperCase());
					System.out.println("enter the publish date");
					String date = scan.next();
					LocalDate e = LocalDate.parse(date);
					Book book = new Book(name, pages, price, type1, e);
					bookService.add(book);
				} catch (Exception e) {
					System.out.println("Invalid Input");
				}
				System.out.println("continue...");

				break;

			case 2:
				bookService.displayAll();

				break;

			case 3:
				System.out.println("enter the name of book to search");
				String name = scan.next();
				bookService.search(name);
				break;

			case 4:
				bookService.sort();
				break;
			case 5:
				System.out.println("enter the pubdate of book to search");
				String date = scan.next();
				LocalDate pubdate = LocalDate.parse(date);
				bookService.search(pubdate);
				List<Book> book = bookService.search(pubdate);
				System.out.println(book);
				if (book == null)
					System.out.print("No Data Found");
				break;
			case 6: {
				try {
					bookService.downloadCSVfile();
				} catch (Exception exception) {
					exception.printStackTrace();
				}
			}
				break;
			case 7: {
				try {
					bookService.downloadJson();
				} catch (Exception exception) {
					exception.printStackTrace();
				}
			}
			case 8:
				System.out.println("enter the name of book to delete");
				String namee = scan.next();
				bookService.delete(namee);
				break;
			case 9:
				System.out.println("exited");
				System.exit(0);
			default:
				System.out.println("Invalid Entry");
				break;
			}

		}
	}

}